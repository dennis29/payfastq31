<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTheadminsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('theadmins', function (Blueprint $table) {
            $table->bigIncrements('id');
			$table->string('email',100)->unique();
            $table->string('password')->nullable();
			$table->string('username')->nullable();
            $table->integer('role_id')->nullable();
			$table->integer('is_active')->nullable();
			$table->timestamp('email_verified_at')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('theadmins');
    }
}

@extends('layouts_main.app')
@section('content')
    <div style="text-align:center;margin-top:10px;">
	   {!! $data['list'] !!}	   
    </div>
@endsection
@section('content_js')
<script src="{{asset('js/knockout-min.js')}}" type="text/javascript"></script>
<script>

 $( function() {

 })
 
function validate(evt) {
  var theEvent = evt || window.event;

  // Handle paste
  if (theEvent.type === 'paste') {
      key = event.clipboardData.getData('text/plain');
  } else {
  // Handle key press
      var key = theEvent.keyCode || theEvent.which;
      key = String.fromCharCode(key);
  }
  var regex = /[0-9]|\./;
  if( !regex.test(key) ) {
    theEvent.returnValue = false;
    if(theEvent.preventDefault) theEvent.preventDefault();
  }
}

 
 function changeQty(incr=1,id=1, price=1, evt){
	    
  	 tot = 0;
	 
	 if(incr=="")
	 {
		 incr = 3;
	 }
	 if(price=="")
	 {
		 price = 3;
	 }
	 $('#price-p-'+id).html('$'+(parseInt(incr)*parseInt(price)));
	 
	 $('.cartSection').find('.p-price').each(function() {
        tot += parseInt(($(this).html()).replace('$',''));
     });
	 
	 $('.specialContent').html('Total : $'+tot);
	 
	 $.ajax({
												  async: true,
												  headers: {
													'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
												  },
												  type: "POST",
												  url: "{{url('api/item/insert_incr')}}",
												  data: "id="+id+"&incr="+incr, 
												  success: function(data){
													   $('#cart-incr').attr('data-count',data.incr);
												  }
	});	
	 
	 
 }
 
 var viewModel = {
		validate: function(data, event) {
            var theEvent = event || window.event;

			  // Handle paste
			  if (theEvent.type === 'paste') {
				  key = event.clipboardData.getData('text/plain');
			  } else {
			  // Handle key press
				  var key = theEvent.keyCode || theEvent.which;
				  key = String.fromCharCode(key);
			  }
			  var regex = /[0-9]|\./;
			  if( !regex.test(key) ) {
				theEvent.returnValue = false;
				if(theEvent.preventDefault) theEvent.preventDefault();
			  }
        },
        changeQty: function(incr=1,id=1, price=1, evt) {
            tot = 0;
	 
			 if(incr=="")
			 {
				 incr = 3;
			 }
			 if(price=="")
			 {
				 price = 3;
			 }
			 $('#price-p-'+id).html('$'+(parseInt(incr)*parseInt(price)));
			 
			 $('.cartSection').find('.p-price').each(function() {
				tot += parseInt(($(this).html()).replace('$',''));
			 });
			 
			 $('.specialContent').html('Total : $'+tot);
			 
			 $.ajax({
														  async: true,
														  headers: {
															'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
														  },
														  type: "POST",
														  url: "{{url('api/item/insert_incr')}}",
														  data: "id="+id+"&incr="+incr, 
														  success: function(data){
															   $('#cart-incr').attr('data-count',data.incr);
														  }
			});	
			 
        }
		
    };
    ko.applyBindings(viewModel);
</script>
@endsection
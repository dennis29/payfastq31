@extends('layouts_main.app')
@section('content')
<style>
         
         .form-signin {
            max-width: 330px;
            padding: 15px;
            margin: 0 auto;
            color: #017572;
         }
         
         .form-signin .form-signin-heading,
         .form-signin .checkbox {
            margin-bottom: 10px;
         }
         
         .form-signin .checkbox {
            font-weight: normal;
         }
         
         .form-signin .form-control {
            position: relative;
            height: auto;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
            padding: 10px;
            font-size: 16px;
         }
         
         .form-signin .form-control:focus {
            z-index: 2;
         }
         
         .form-signin input[type="email"] {
            margin-bottom: -1px;
            border-bottom-right-radius: 0;
            border-bottom-left-radius: 0;
            border-color:#017572;
         }
         
         .form-signin input[type="password"] {
            margin-bottom: 10px;
            border-top-left-radius: 0;
            border-top-right-radius: 0;
            border-color:#017572;
         }
         
         h2{
            text-align: center;
            color: #017572;
         }
      </style>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"> <h2 style="margin-top:20px">{{ __('Customer Login') }}</h2></div>

                <div class="card-body">
                    <form method="POST" action="{{ route('customer.login.submit') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6 offset-md-4">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div>
                        </div>
						<div class="row justify-content-md-center">
                            <div class="col-md-auto offset-md-14">
                                <div class="form-check">
                                    <label class="label">
                                        {{ __("Don't have an account. ") }} <a href="{{ route('customer.register') }}">
                                       {{ __('Sign Up') }}
                                    </a>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="row justify-content-md-center">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Login') }}
                                </button>
								@if (Route::has('password.request'))
									<a class="btn btn-block btn-social" href="{{ route('customer.password.request') }}" style='margin-top:50px;font-size:12px;text-align:center;background:#415546'>
                                       <i class="fa fa-question"></i>{{ __('Forgot Your Password?') }}
                                    </a>
								@endif
								@if (Route::has('social_log'))
                                    <a class="btn btn-block btn-social" href="{{ url('/auth/redirect/facebook') }}" style='margin-top:50px;height:50px;font-size:12px;text-align:center;background:#337ab7'>
                                       <i class="fa fa-facebook" style="font-size:25px;"></i>{{ __('Login With Facebook') }}
                                    </a>
									<a class="btn btn-block btn-social" href="{{ url('/auth/redirect/google') }}" style='margin-top:50px;height:50px;font-size:12px;text-align:center;background:#d71d2e'>
                                       <i class="fa fa-google" style="font-size:25px;"></i>{{ __('Login With Google') }}
                                    </a>
									
								<!--	<a class="btn btn-block btn-social" href="{{ url('/auth/redirect/github') }}" style='margin-top:50px;height:50px;font-size:12px;text-align:center;background:#085320d6'>
                                       <i class="fa fa-github" style="font-size:25px;"></i>{{ __('Login With Github') }}
                                    </a>	
									<a class="btn btn-block btn-social" href="{{ url('/auth/redirect/gitlab') }}" style='margin-top:50px;height:50px;font-size:12px;text-align:center;background:#df150ad6'>
                                       <i class="fa fa-gitlab" style="font-size:25px;"></i>{{ __('Login With Gitlab') }}
                                    </a>
									<a class="btn btn-block btn-social" href="{{ url('/auth/redirect/bitbucket') }}" style='margin-top:50px;height:50px;font-size:12px;text-align:center;background:#163280d6'>
                                       <i class="fa fa-bitbucket" style="font-size:25px;"></i>{{ __('Login With Bitbucket') }}
                                    </a>
								-->	
                                @endif
								
                            </div>
                        </div>
                    </form>
					
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('content_css')
<link rel='stylesheet' type='text/css' href="{{asset('css/bootstrap-social.css')}}"> 
@endsection
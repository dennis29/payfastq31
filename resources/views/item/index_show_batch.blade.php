@extends('layouts.backend')

@section('content')
    <div class="container">
        <div class="row">
            @include('admin.sidebar')

            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">Item Batch Upload</div>
                    <div class="card-body">
                        <form style="border: 4px solid #a1a1a1;margin-top: 15px;padding: 10px;" action="{{ url('itemBatchUpload') }}" class="form-horizontal" method="post" enctype="multipart/form-data">
							                @csrf

								<input type="file" name="file" class="form-control">

								<br>

								<button class="btn btn-success">Import Item Data</button>
						</form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

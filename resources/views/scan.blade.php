@extends('layouts_main.app')
@section('content')
        <div style="text-align:center;margin-top:10px;">  
			<div class="row justify-content-md-center" style="margin-bottom:50px !important">
				<div class="col-md-auto">
				  <h2 class="projTitle"><a href = "item/shopping_chart"  class="label label-success" >View My Cart</a></h2>
				</div>
			</div>
			<div class="row justify-content-md-center" style="!important">
				<div class="col-md-auto">
				  <h2><i>Scan the barcode to add to cart</i></h2>
				</div>
			</div>		    
        <div style="display:none !important" >
                <form method="GET" action="schandler.php" id="addItem">   
                    Item Code: <input type="text" id="itemcode" name="itemcode"><br/>
                    Item Qty: <input type="text" id="itemqty" name="itemqty">
                </form>
                <br/>
                <button type="submit" form="addItem" value="Submit">Add to cart</button> 
        </div>
        </div>
		<section id="container" class="container">
        <div class="controls" style="display:none !important">
            <fieldset class="input-group">
                <button class="stop">Stop</button>
            </fieldset>
            <fieldset class="reader-config-group">
                <label>
                    <span>Barcode-Type</span>
                    <select name="decoder_readers">
                        <option value="code_128" selected="selected">Code 128</option>
                        <option value="code_39">Code 39</option>
                        <option value="code_39_vin">Code 39 VIN</option>
                        <option value="ean">EAN</option>
                        <option value="ean_extended">EAN-extended</option>
                        <option value="ean_8">EAN-8</option>
                        <option value="upc">UPC</option>
                        <option value="upc_e">UPC-E</option>
                        <option value="codabar">Codabar</option>
                        <option value="i2of5">Interleaved 2 of 5</option>
                        <option value="2of5">Standard 2 of 5</option>
                        <option value="code_93">Code 93</option>
                    </select>
                </label>
                <label>
                    <span>Resolution (width)</span>
                    <select name="input-stream_constraints">
                        <option value="320x240">320px</option>
                        <option selected="selected" value="640x480">640px</option>
                        <option value="800x600">800px</option>
                        <option value="1280x720">1280px</option>
                        <option value="1600x960">1600px</option>
                        <option value="1920x1080">1920px</option>
                    </select>
                </label>
                <label>
                    <span>Patch-Size</span>
                    <select name="locator_patch-size">
                        <option value="x-small">x-small</option>
                        <option value="small">small</option>
                        <option selected="selected" value="medium">medium</option>
                        <option value="large">large</option>
                        <option value="x-large">x-large</option>
                    </select>
                </label>
                <label>
                    <span>Half-Sample</span>
                    <input type="checkbox" checked="checked" name="locator_half-sample" />
                </label>
                <label>
                    <span>Workers</span>
                    <select name="numOfWorkers">
                        <option value="0">0</option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option selected="selected" value="4">4</option>
                        <option value="8">8</option>
                    </select>
                </label>
                <label>
                    <span>Camera</span>
                    <select name="input-stream_constraints" id="deviceSelection">
                    </select>
                </label>
                <label style="display: none !important">
                    <span>Zoom</span>
                    <select name="settings_zoom"></select>
                </label>
                <label style="display: none !important">
                    <span>Torch</span>
                    <input type="checkbox" name="settings_torch" />
                </label>
            </fieldset>
        </div>
      <div id="result_strip" style="display:none">
        <ul class="thumbnails"></ul>
        <ul class="collector"></ul>
      </div>
      <div id="interactive" class="viewport text-center"></div>
	<div class="modal fade" id="modal_popup" tabindex="-1" role="dialog" aria-labelledby="modal_popupLabel" aria-hidden="true">
		  <div class="modal-dialog" role="document">
			<div class="modal-content">
			  <div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Item Barcode</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				  <span aria-hidden="true">&times;</span>
				</button>
			  </div>
			  <div class="modal-body">
				<form id="form_item" style="font-weight:bold !important">
    			  <div class="form-group">
					  <div class="text-center">
						<img src="" id="img" style="display:none" class="form-control !important">
					  </div>
				  </div>
				  <div class="form-group">
					<label for="name">Name</label>
					<input class="form-control" id="name" readonly="readonly">
				    <input type="hidden" class="form-control" id="id">
				  </div>
				  <div class="form-group">
					<label for="code">Bar Code</label>
					<input class="form-control" id="code" readonly="readonly">
				  </div>
				  <div class="form-group">
					<label for="sku">SKU</label>
					<input class="form-control" id="sku" readonly="readonly">
				  </div>
				  <div class="form-group">
					<label for="description">Description</label>
					<input class="form-control" id="description" readonly="readonly">
				  </div>
				  <div class="form-group">
					<label for="price">Price</label>
					<input class="form-control" id="price" readonly="readonly">
				  </div>
				</form>
			  </div>
			  <div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal" >Close</button>
				<button type="button" class="btn btn-primary" id="add_to_chart">Add To Chart</button>
			  </div>
			</div>
		  </div>
    </div>
@endsection
@section('content_js')
    <script src="{{asset('js/adapter-latest.js')}}" type="text/javascript"></script>
    <script src="{{asset('js/quagga.js')}}" type="text/javascript"></script>
    <script src="{{asset('js/live_w_locator.js')}}" type="text/javascript"></script>
	<script src="{{asset('js/jquery-3.4.1.min.js')}}"></script>
	<script type="text/javascript">
	function add_item(str=""){
			  $.ajax({
				  async: false,
				  headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				  },
				  type: "POST",
				  url: "api/item/get_barcode",
				  data: "barcode="+str,
				  success: function(data){
					  console.log(data);
					  
					 if(Object.keys(data).length > 0)
					 {
						$.each(data, function( k, v ) {
						  $("#form_item #"+k).val(v);
						});
						$('#img').attr("src",data.img);
						$('#img').attr("style","display:inline-block;width:200px;height:200px");
						$('#modal_popup').modal('show');

					 } 
					 else
					 {
						 $.alert({
							title: 'Confirmation',
							content: 'This barcode not found',
						})
					 }
				  }
			  });
	}
	
	function add_to_chart(){
			  $.ajax({
				  async: false,
				  headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				  },
				  type: "POST",
				  url: "api/item/add_to_chart",
				  data: "id="+$("#form_item #id").val(),
				  success: function(data){
					  $('#cart-incr').attr('data-count',data.incr);
						$.when($.alert({
							title: 'Confirmation',
							content: 'Successfully add To Cart',
						})).then($('#modal_popup').modal('hide'));

				  }
			  });
	}
	
	$( "#add_to_chart" ).click(function() {
		  add_to_chart();
	});
	$( function() {
		
		 
	})
	</script>
@endsection
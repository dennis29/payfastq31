<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>PayFastQ</title>
    <!-- Styles -->
<head>	
    <link rel="shortcut icon" type="image/x-icon" href="{{asset('images/logo_.jpg')}}" />
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
	<link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="{{asset('css/styles.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap.min.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap-theme.min.css')}}">
	<link href="{{ asset('css/jquery-confirm.min.css') }}" rel="stylesheet">
	<link rel='stylesheet' type='text/css' href="{{asset('css/cart_styles.css')}}"> 
	<link rel='stylesheet' type='text/css' href="{{asset('css/font-awesome.css')}}"/> 
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	@yield('content_css')
	<style>
			h2{
				text-align: center;
				color: #017572;
			}
			#ex4 .p1[data-count]:after{
			  position:absolute;
			  right:10%;
			  top:8%;
			  content: attr(data-count);
			  font-size:40%;
			  padding:.2em;
			  border-radius:50%;
			  line-height:1em;
			  color: white;
			  background:rgba(255,0,0,.85);
			  text-align:center;
			  min-width: 1em;
			  //font-weight:bold;
			}
		</style>
</head>

</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container">
               <a href="{{ route('main') }}"><img src="{{ asset('images/logo.jpg') }}" style='width:600px' /></a>
			   <div class="container" style='margin-bottom:20px;margin-top:20px;color:blue'>	   
				  <div class="row justify-content-md-center">
					<div class="col-md-auto">
					  @if(session('store'))		
					  <h4 style='color:green'>You are shopping at {{session('store')}}</h4>
					   @endif 
					</div>
				  </div>
				</div>
				<div id="ex4">
				@php if(!isset($data['incr'])){$data['incr'] = 0;} @endphp
				  <a  href="{{ route('shopping_chart') }}"><span class="p1 fa-stack fa-2x has-badge" data-count="{{$data['incr']}}" id='cart-incr' style='color:black'>
					<!--<i class="p2 fa fa-circle fa-stack-2x"></i>-->
					<i class="p3 fa fa-shopping-cart fa-stack-1x xfa-inverse" data-count="4b"></i>
				  </span></a>
				</div>
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->


                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
						<li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    My Item <span class="caret"></span>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
								    <a class="dropdown-item" href="{{ route('main') }}">
                                        {{ __('My Store') }}
                                    </a>
                                    <a class="dropdown-item" href="{{ route('scan_data') }}">
                                        {{ __('My Scan') }}
                                    </a>
									<a class="dropdown-item" href="{{ route('shopping_chart') }}">
                                        {{ __('My Cart') }}
                                    </a>
									<a class="dropdown-item" href="{{ route('my_payment') }}">
                                        {{ __('My Payment') }}
                                    </a>
                                </div>
                            </li>
						
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('customer.login') }}">{{ __('Login') }}</a>
                            </li>
                            @if (Route::has('customer.register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('customer.register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('customer.home') }}">
                                        {{ __('My Transaction') }}
                                    </a>
									<a class="dropdown-item" href="{{ route('scan_trans') }}">
                                        {{ __('Scan Transaction') }}
                                    </a>
									<a class="dropdown-item" href="{{ route('customer.logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('customer.logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">
            @yield('content')
        </main>
    </div>
<!-- Footer -->
@include('layouts.footer')
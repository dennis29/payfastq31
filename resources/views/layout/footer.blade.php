<!-- Footer -->
<footer class="page-footer fixed" style="margin-top:50px;height:100%;width:100%;display:inline-block">
  <!-- Copyright -->
  <div class="footer-copyright text-center py-3">© 2019 Copyright : FastQ
  </div>
  <!-- Copyright -->
</footer>
<!-- Footer -->
<script src="{{asset('js/jquery-1.9.0.min.js')}}"></script>
@yield('content_js')	
<script src="{{asset('js/bootstrap.min.js')}}"></script> 
<script type="text/javascript" src="{{ asset('js/jquery-confirm.min.js') }}"></script>
</body>
</html>
<!DOCTYPE html>
<html>

<head>
	<link rel="stylesheet" type="text/css" href="{{asset('css/styles.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap.min.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap-theme.min.css')}}">
	<link href="{{ asset('css/jquery-confirm.min.css') }}" rel="stylesheet">
	<link rel='stylesheet' type='text/css' href="{{asset('css/cart_styles.css')}}"> 
	@yield('content_css')
</head>
<style>
h2{
    text-align: center;
    color: #017572;
}
</style>
<body>
<meta name="csrf-token" content="{{ csrf_token() }}"> 	
<div style="text-align:center;margin-top:10px;">
           <a href="index.php"><img src="{{ asset('images/logo.jpg') }}"/></a>
</div>
<div class="container" style='margin-bottom:20px;margin-top:20px;color:blue'>
  <div class="row justify-content-md-center">
    <div class="col-md-auto">
      <a href="{{url('login')}}" class="continue btn btn-primary btn-sm" style='margin-left:30px;background:#7b50b0'>Login Here</a>
	  <a href="{{url('register')}}" class="continue btn btn-primary btn-sm" style='background:#7b50b0'>Register Here</a>
    </div>
  </div>
</div>
@if (session('store'))
	<header class="page-footer font-small special-color-dark pt-4" style="height:100%;width:100%;display:inline-block">
	  <!-- Copyright -->
	  <div class="footer-copyright text-center py-3"><h2>You are shopping at {{session('store')}}</h2>
	  </div>
	  <!-- Copyright -->
	</header>
@endif
@extends('layouts.backend')

@section('styles')

<link href="{{asset('assets/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
<link href="{{asset('assets/fontawesome-free/css/all.min.css')}}" rel="stylesheet">
<link href="{{asset('css/sb-admin.css')}}" rel="stylesheet">

@endsection


@section('content')
    <div class="container">
        <div class="row">
            @include('admin.sidebar')

            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">Dashboard</div>

                    <div class="card-body">
                        Welcome {{ Auth::user()->name }} to FastQ Administration
                    </div>
					<div id="wrapper">


			<div id="content-wrapper">

				<div class="container-fluid">

				<!-- 
				karena ini halaman overview (home), kita matikan partial breadcrumb.
				Jika anda ingin mengampilkan breadcrumb di halaman overview,
				silahkan hilangkan komentar (//) di tag PHP di bawah.
				-->
				<?php //$this->load->view("admin/_partials/breadcrumb.php") ?>

				<!-- Icon Cards-->
				<div class="row">
					<div class="col-xl-3 col-sm-6 mb-3">
					<div class="card text-white bg-primary o-hidden h-100">
						<div class="card-body">
						<div class="card-body-icon">
							<i class="fas fa-fw fa-comments"></i>
						</div>
						<div class="mr-5">26 New Messages!</div>
						</div>
						<a class="card-footer text-white clearfix small z-1" href="#">
						<span class="float-left">View Details</span>
						<span class="float-right">
							<i class="fas fa-angle-right"></i>
						</span>
						</a>
					</div>
					</div>
					<div class="col-xl-3 col-sm-6 mb-3">
					<div class="card text-white bg-warning o-hidden h-100">
						<div class="card-body">
						<div class="card-body-icon">
							<i class="fas fa-fw fa-list"></i>
						</div>
						<div class="mr-5">11 New Tasks!</div>
						</div>
						<a class="card-footer text-white clearfix small z-1" href="#">
						<span class="float-left">View Details</span>
						<span class="float-right">
							<i class="fas fa-angle-right"></i>
						</span>
						</a>
					</div>
					</div>
					<div class="col-xl-3 col-sm-6 mb-3">
					<div class="card text-white bg-success o-hidden h-100">
						<div class="card-body">
						<div class="card-body-icon">
							<i class="fas fa-fw fa-shopping-cart"></i>
						</div>
						<div class="mr-5">123 New Orders!</div>
						</div>
						<a class="card-footer text-white clearfix small z-1" href="#">
						<span class="float-left">View Details</span>
						<span class="float-right">
							<i class="fas fa-angle-right"></i>
						</span>
						</a>
					</div>
					</div>
					<div class="col-xl-3 col-sm-6 mb-3">
					<div class="card text-white bg-danger o-hidden h-100">
						<div class="card-body">
						<div class="card-body-icon">
							<i class="fas fa-fw fa-life-ring"></i>
						</div>
						<div class="mr-5">13 New Tickets!</div>
						</div>
						<a class="card-footer text-white clearfix small z-1" href="#">
						<span class="float-left">View Details</span>
						<span class="float-right">
							<i class="fas fa-angle-right"></i>
						</span>
						</a>
					</div>
					</div>
				</div>

				<!-- Area Chart Example-->
				<div class="card mb-3">
					<div class="card-header">
					<i class="fas fa-chart-area"></i>
					Visitor Stats</div>
					<div class="card-body">
					<canvas id="myAreaChart" width="537" height="161" style="display: block; width: 537px; height: 161px;" class="chartjs-render-monitor"></canvas>
					</div>
					<div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
				</div>

				</div>

			</div>
			<!-- /.content-wrapper -->

		</div>
                </div>
            </div>
        </div>
    </div>
	<!-- Bootstrap core JavaScript-->

@endsection

@section('scripts')
<!-- Bootstrap core JavaScript-->
<script src="{{asset('jquery/jquery.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>
<script src="{{asset('js/demo/chart-area-demo.js')}}"></script>
@endsection
@extends('layouts_main.app')
@section('content')
    <div style="text-align:center;margin-top:10px;margin-bottom:100px">
		<br/><br/><h1 class="projTitle">Payment made successfully!</h1><br/>
		<div id="paypal-button-container"></div>
		<div style="text-align:center">
			<h3>You can bring the items out of the store now.</h3>
		</div>
		<div class="panel-body text-center">
            		    <div class="text-center">
							<span class = "glyphicon glyphicon-download"></span><span class = "glyphicon glyphicon-download"></span>&nbsp;&nbsp;Download and use this struct to see transactions:&nbsp;
							<span class = "glyphicon glyphicon-download"></span><span class = "glyphicon glyphicon-download"></span>
							<br/><a download="{{$data}}.jpg" href="{{asset('gbr/')}}/{{$data}}.jpg" title="{{$data}}.jpg">
							<img alt="ImageName" src="{{asset('gbr/')}}/{{$data}}.jpg"/></a>
						</div>
        </div>
	</div>
@endsection
@extends('layouts_main.app')
@section('content')
<div class="container col-md-12">
    <div class="row justify-content-center col-md-12" style="margin-right:-200px;width:2000px !important">
        <div class="col-md-12" >
            <div class="card">
                <div class="card-header col-md-12">Customer Transaction</div>

                <div class="card-body ">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <table class="table">
					  <thead class="thead-dark">
						<tr>
						  <th scope="col">#</th>
						  <th scope="col">Action</th>
						  <th scope="col">Store</th>
						  <th scope="col">Item(s)</th>
						  <th scope="col">Price</th>
						  <th scope="col">Total</th>
						  <th scope="col">Payment Method</th>
						</tr>
					  </thead>
					   @if(count($data) == 0)
						<tr>
						  <td colspan=7 style='text-align:center'>No data found</td>
						</tr>               						
					   @endif
					  <tbody>
					@if($data)
					  @foreach($data as $k=>$v)
						<tr>
						  <th scope="row">{{$k+1}}</th>
						   <td><button type="submit" class="btn btn-primary" onclick='gotoPdf({{$v->id}})'>Print</button></td>
						  <td>{{$v->store_name}}</td>
							@php 
							  $item = '';
							  $incr = explode(',',$v->incr);
							  $price = explode(',',$v->price);
							  $price_x = "";
							  $i=0;
							  $tot=0;
							  foreach(json_decode($v->item) as $m)
							  {
								  $item .= "[".$m." x ".$incr[$i]."] ";
								  $price[$i]  = $price[$i]*$incr[$i];
								  $price_x .= "[".$price[$i]."] + ";
								  $tot += $price[$i];
								  $i++;
							  }
							@endphp
						  <td>{{$item}}</td>
						  <td>{{substr($price_x, 0, -2)}}</td>
						  <td>{{$tot}}</td>
						  <td>{{$v->payment}}</td>
						</tr>
					  @endforeach
                     @endif 					  
					  </tbody>
					</table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('content_js')
<script>
$('#footer-footer').attr('style',$('#footer-footer').attr('style')+';width:2000px');
function gotoPdf(id){
	
	window.location = "{{url('trans/exportTransPDF')}}"+"/"+id;
}
</script>
@endsection
<!-- {{Auth::user()}}
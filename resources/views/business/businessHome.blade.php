@extends('layouts.app_business')
@section('css')
  <!-- Bootstrap core CSS -->
  <link href="{{asset('vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="{{asset('css/business-frontpage.css')}}" rel="stylesheet">
  <style>
  .navbar-laravel{
	  margin-bottom:-24px
  }
  </style>
@endsection  
@section('content')
  <!-- Navigation -->

  <!-- Header -->
  <header class="bg-primary py-5 mb-5">
    <div class="container h-100">
      <div class="row h-100 align-items-center">
        <div class="col-lg-12">
          <h4 class="display-4 text-white mt-4 mb-4">{{$company->name}}</h4>
          <p class="lead mb-5 text-white-50"><img src="{{$company->url}}"  style="width:100px;box-shadow: 2px 2px grey;"></p>        
		</div>
      </div>
    </div>
  </header>

  <!-- Page Content -->
  <div class="container">

    <div class="row">
      <div class="col-md-8 mb-5" style="width:2000px">
        <table class="table">
    <thead>
      <tr>
        <th>#</th>
        <th>Item Name</th>
        <th>Code</th>
		<th>SKU</th>
		<th>Description</th>
		<th>Price</th>
		<th>Image</th>
      </tr>
    </thead>
    <tbody>
	@php 
		$count = 0;
	@endphp
	@if($data)
		@foreach($data as $k=>$v)
		@php 
			$count++;
		@endphp
		  <tr>
			<td>{{$count}}</td>
			<td>{{$v->name}}</td>
			<td>{{$v->code}}</td>
			<td>{{$v->sku}}</td>
			<td>{{$v->description}}</td>
			<td style="text-align:right">{{$v->price}}</td>
			<td><img src="{{$v->img}}"  style="width:40px;box-shadow: 2px 2px grey;"></td>
		  </tr>
		@endforeach    
    @endif		
    </tbody>
  </table>
      </div>
    </div>
    <!-- /.row -->

  </div>
  <!-- /.container -->


  <!-- Bootstrap core JavaScript -->
  <script src="{{asset('vendor/jquery/jquery.min.js')}}"></script>
  <script src="{{asset('vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
@endsection
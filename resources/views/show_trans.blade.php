				<h2>FastQ</h2>
				@if($data['store'])			   
				  <h4>You were shopping at {{$data['store']}}</h4>
				@endif
        		<table border=2 style='width:730px'>
				  <thead>
					<tr>
					  <th>#</th>
					  <th>Item</th>
					  <th>Decription</th>
					  <th> x </th>
					  <th>Price</th>
					</tr>
				  </thead>
				  <tbody>
				  @php 
				  $incr = explode(',',$data['incr']);
				  $tot = 0;
				  $price = explode(',',$data['price']);		
				  $description = json_decode($data['description']);
				  $k=0;
				  
				  @endphp
				  
				  @foreach(json_decode($data['item']) as $m)
					<tr>
					  <th>{{$k+1}}</th>
					  <td>{{$m}}</td>
					  <td>{{$description[$k]}}</td>
					  <td style="text-align:right"> x {{$incr[$k]}}</td>
					  <td style="text-align:right">{{$price[$k]*$incr[$k]}}</td>
					</tr>
					  @php $tot+=$price[$k]*$incr[$k];$k++; @endphp
				  @endforeach			  
				  </tbody>
				  <tfoot>
				    <tr>
					  <th style="text-align:right" colspan=5>Total : {{$tot}}</th>
				   </tr>	
				   <tr>
					  <th style="text-align:right" colspan=5>You are using {{$data['payment']}}</th>
				   </tr>
				  </tfoot>
				</table>
			
<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */
	'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],
    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],
    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => env('SES_REGION', 'us-east-1'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
        'webhook' => [
            'secret' => env('STRIPE_WEBHOOK_SECRET'),
            'tolerance' => env('STRIPE_WEBHOOK_TOLERANCE', 300),
        ],
    ],
	
	'facebook' => [
     'client_id' => '2221754014617259',
     'client_secret' => '4b2ca4cdd1a0db199b2b431ccff948f0',
     'redirect' => "/callback/facebook",
   ], 
   'google' => [
        'client_id' => '913986054495-0uab88nqf8ab04dstrvruiml6gd0fs6s.apps.googleusercontent.com',
        'client_secret' => 'mdey77_JVUVjA7BWXXeZNVbc',
        'redirect' => '/callback/google',
    ],
	'github' => [
        'client_id' => '53077e0cab6823722d7c',
        'client_secret' => 'f6881ef64a7f060ebdca317bb1b65acc1d8c25c1',
        'redirect' => '/callback/github',
    ],
	'gitlab' => [
        'client_id' => '6b0f8d705a898be715e48cd783763be6d1b2881dd0a2c7b5e4bc78656fd77e73',
        'client_secret' => '4d7b306dd9517c7dc474f20fcb0e3892a584221beb0b5d356c2f9f7042ac0b84',
        'redirect' => '/callback/gitlab',
    ],
	'bitbucket' => [
        'client_id' => '73LJm55GaWtV2ERr6k',
        'client_secret' => 'MNHtJRuT7JqQD3AYF2VwVW5Z7aXNt9Xn',
        'redirect' => '/callback/bitbucket',
    ]
	
];

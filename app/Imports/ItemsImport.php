<?php

namespace App\Imports;

use App\items;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;

class ItemsImport implements ToModel, WithStartRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new items([
		    'company_id' => $row[0],
            'code'     => $row[1],
            'sku'    => $row[2], 
			'name'    => $row[3],
			'description' => $row[4],
			'price'    => $row[5],
			'img'    => $row[6],
        ]);
    }
	public function startRow(): int
    {
        return 2;
    }
}

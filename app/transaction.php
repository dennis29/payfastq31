<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class transaction extends Model
{
    protected $fillable = [
        'price', 'item', 'description' , 'incr', 'id_user' ,'store' , 'payment'
    ];
	
	//protected  $primaryKey = 'id';
}

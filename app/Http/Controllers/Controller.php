<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
	
	function set_session($request=array())
	{
		$data = array();
		
		if (!$request->session()->has('store')) {
		   $request->session()->put('store', '');
		}
		
		if (!$request->session()->has('company_id')) {
		   $request->session()->put('company_id', '');
		}
		
		if ($request->get("store")) {
		   $request->session()->put('store', ($request->get("store")));
		}
		
		if ($request->get("company_id")) {
		   $request->session()->put('company_id', ($request->get("company_id")));
		}
		
		if (!$request->session()->has('incr')) {
		   $request->session()->put('incr', []);
		}	
		
		$incr = $request->session()->get('incr');
		
		$data['incr'] = 0;
		if($incr )
		{
			foreach($incr as $v)
			{
				if(isset($v))
				{
				   $data['incr'] += $v; 	
				}
			}
		}
		
		$data['store'] = $request->session()->get('store');
		
		return $data;
		
	}
}

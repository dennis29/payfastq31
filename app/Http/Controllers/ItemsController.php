<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\items;

class ItemsController extends Controller
{
    function get_barcode(Request $request){
		return response()->json(Items::where('code','=',$request->post('barcode'))->where('company_id','=',$request->session()->get('company_id'))->first());
	}
	
	function add_to_chart(Request $request)
	{
		if (!$request->session()->has('chart')) {
		   $request->session()->put('chart', []);
		}	
		
		if (!$request->session()->has('incr')) {
		   $request->session()->put('incr', []);
		}
		
		$data = $request->session()->get('chart');
		$incr = $request->session()->get('incr');
		
		$data[] = $request->post('id');	
		$incr[] = 3;
		
		$request->session()->put('chart', $data);
		$request->session()->put('incr', $incr);
		//dd(urlencode($request->get("store")));
		
		$data = $this->set_session($request);
		
		return response()->json($data);
		
	}
	
	function insert_incr(Request $request)
	{
		$incr = $request->session()->get('incr');
		
		$incr[$request->post('id')] = $request->post('incr');	
		
		$request->session()->put('incr', $incr);
		
		$data = $this->set_session($request);
		
		return response()->json($data);	
	}
	
	function shopping_chart(Request $request)
	{ 
		$var = "";
		$var .= "<div class=\"heading cf\">";
		$var .= "<a href='../scan' class=\"continue\">Add more items</a>  ";		
		$var .= "<a href='payment' class=\"continue\">Check Out and Pay</a>";
		$var .= "</div>";
		$var .= "<h1 class=\"projTitle\">Your Shopping Cart</h1>";
			
		$data = $request->session()->get('chart');	
		$incr = $request->session()->get('incr');	
		if ($data) {
			// output data of each row
			$var .= "<div class=\"cart\">";
			$var .= "<ul class=\"cartWrap\" style=\"padding-right: 40px;\">";	
			$c=0;
			$tot=0;
			foreach($data as $v)
			{
						$row = Items::where('id','=',$v)->first();
						$tot += $row["price"]*$incr[$c];
						$var .= "<li class=\"items even\">";
						$var .= "<div class=\"infoWrap\">";
						$var .= "<div class=\"cartSection info\">";
						$var .= "<img src='".$row["img"]."' alt='' class='itemImg'/>";
						$var .= "<p class=\"itemNumber\">". $row["itemcode"] ."</p>";
						$var .= "<a href=\"schandler.php?itemcode=". $row["itemcode"]."&itemqty=1\" style=\"text-decoration:none;color:#777\"><h3>". $row["name"]."</h3></a>";
						$var .= "<p> <input id='price-".$c."' onkeyup='changeQty(this.value,".$c.",".$row["price"].")' onkeypress='validate(event)' type='text'  class='qty' placeholder='".$incr[$c]."' /> x $".$row["price"]."</p>";
						$var .= "<br/> <p class=\"stockStatus\"> In Stock</p>";
						$var .= "</div>";
						
						$var .= "<div class=\"prodTotal cartSection\">";
						$var .= "<p class='p-price' id='price-p-".$c."'>$".($row["price"]*$incr[$c])."</p>";
						$var .= "</div>";
						
						
						$var .= "<div class=\"cartSection removeWrap\">";
						$var .= "<a href='".url('trans/remove').'/'.$c."'\" class=\"remove\">x</a>";
						$var .= "</div>";
						$var .= "</div>";
						
						$var .= "<div class=\"special\"><div class=\"specialContent\">Free gift and points with purchase!</div></div>";
						$var .= "</li>";
						$c++;
				} 

				$var .= "</ul>";
				$var .= "</div>";
				$var .= "<div class=\"prodTotal cartSection\"><div class=\"specialContent\">Total : $".$tot."</div></div>";
				$request->session()->put('tot', $tot);
			
		} else {
			$var .= "0 results";
		}
		
		$data = $this->set_session($request);
		
		$data['list'] = $var;
		
		return view('shopping_chart')->with('data', $data);
	}
	
	function payment(Request $request)
	{
		$chart = $request->session()->get('chart');	
		$incr = $request->session()->get('incr');	
		
		$k=0;
		$tot = 0;
		
		$data = $this->set_session($request);
		
		if($chart)
		{
			foreach($chart as $v)
			{
				$row = Items::where('id','=',$v)->first();
				$tot += $row['price']*$incr[$k];
				$k++;
			}
			
			$data['tot'] = $tot;
		}
		return view('payment')->with('data', $data);
		
	}
	function get_tot(Request $request)
	{
		if (!$request->session()->has('chart')) {
		   $request->session()->put('chart', []);
		}	
		
		
		$data = $request->session()->get('chart');
		
		$data[] = $request->post('id');		
		
		$request->session()->put('chart', $data);
		//dd(urlencode($request->get("store")));
		
		return view('payment')->with('data', $data);
		
	}
}

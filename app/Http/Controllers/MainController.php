<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Session;

use App\Company;

class MainController extends Controller
{
    function view_data(Request $request){
		
		$data = $this->set_session($request);
		$company = company::all();	
		return view("main", compact("data","company"));
	}
	
	function scan_data(Request $request){
		
		$data = $this->set_session($request);
		return view('scan')->with('data', $data);
	}
	
	function login(Request $request){
		
		$data = $this->set_session($request);
		return view('login')->with('data', $data);
	}
	
	function register(Request $request){
		
		$data = $this->set_session($request);
		return view('register')->with('data', $data);
	}
}

<?php
namespace App\Http\Controllers;

use App\transaction;

class CustomerController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:customer');
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		$id = \Auth::user()->id;
		$data = Transaction::all()->where('id_user',$id);
        return view('customer')->with('data', $data);
    }
}
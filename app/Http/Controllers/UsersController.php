<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\users;

class UsersController extends Controller
{
    function check_username(Request $request)
	{
		
		return response()->json(Users::select('id')->where('username','=',$request->post('username'))->first());
		
	}
	
	function check_email(Request $request)
	{
		
		return response()->json(Users::select('id')->where('email','=',$request->post('email'))->first());
		
	}
}

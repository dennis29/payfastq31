<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\theadmin;

use Redirect;

use Session;

class TheadminController extends Controller
{
	
	function theAdminLogin(Request $request)
	{
		return view('srtdash.login');
		
	}
	
	public function theAdminSubmitlogin(Request $request)
	{
		$theadmin = new Theadmin();
		$get_data = $theadmin->logintomenu($request->post());
		//dd($get_data);
	
		if($get_data)
		{
		//	dd($get_data->dir_url);
			if($get_data->username)
			{	
		        $request->session()->put('admin_email', $request->post('email'));
				$request->session()->put('admin_username', $get_data->username);
				//dd($get_data->dir_url);
				return redirect('theAdmin/home');
			}
			else
			{
			    return Redirect::back()->withErrors(['Email or password is wrong/not registered']);
			}
		}
		else
		{
			return Redirect::back()->withErrors(['Email or password is wrong/not registered']);
		}
	
	}
	
	function theAdminHome(Request $request)
	{
		$check_zone_permission =  $request->session()->get('admin_username');
		
		if(!$check_zone_permission){
		 return redirect()->route('theAdmin.login');
		}
		return view('srtdash.index');
		
	}
	
	function theAdminBarchart(Request $request)
	{
		if(!$check_zone_permission){
		 return redirect()->route('theAdmin.login');
		}
		
		return view('srtdash.index2');
		
	}
	
	function theAdminRegister(Request $request)
	{
		if(!$check_zone_permission){
		 return redirect()->route('theAdmin.login');
		}
		
		return view('srtdash.login3');
		
	}
	
	function theAdminResetPass(Request $request)
	{
		if(!$check_zone_permission){
		 return redirect()->route('theAdmin.login');
		}
		return view('srtdash.index');
		
	}
	
	function theAdminPricing(Request $request)
	{
		if(!$check_zone_permission){
		 return redirect()->route('theAdmin.login');
		}
		return view('srtdash.index');
		
	}
	
	function theAdminLogout(Request $request)
	{
		
		$request->session()->flush();
	
		return redirect()->route('theAdmin.login');
		
	}
}

<?php
 namespace App\Http\Controllers;
 use Illuminate\Http\Request;
 use Validator,Redirect,Response,File;
 use Socialite;
 use Auth;
 use App\Customer;
 class SocialController extends Controller
 {
 public function redirect($provider)
 {
     return Socialite::driver($provider)->redirect();
 }
 public function callback($provider)
 {
   $getInfo = Socialite::driver($provider)->user(); 
   $customer = $this->createCustomer($getInfo,$provider); 
   Auth::guard('customer')->login($customer);
   return redirect()->to('/customer/home');
 }
 function createCustomer($getInfo,$provider){
 $customer = Customer::where('provider_id', $getInfo->id)->orWhere('email', $getInfo->email)->first();
 if (!$customer) {
      $customer = Customer::create([
         'name'     => $getInfo->name,
         'email'    => $getInfo->email,
         'provider' => $provider,
         'provider_id' => $getInfo->id
     ]);
   }
   return $customer;
 }
 }
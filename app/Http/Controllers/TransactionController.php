<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\transaction;

use App\items;

use Redirect;

use Illuminate\Support\Facades\Session;

use PDF;

use Illuminate\Support\Facades\Auth;

class TransactionController extends Controller
{
    function insert_data(Request $request)
	{

			$input = array();
			$description = array();
			$price = '';
			$item = array();
			$incrItem = '';
 			$data = $request->session()->get('chart');	
			$incr = $request->session()->get('incr');	
			$id_user =  \Auth::user()->id;
			
			//dd(Auth::user()->id);
			
			if($id_user == "")
			{
				$id_user = 0;
			}
			
			$k=0;
			foreach($data as $v)
			{
			  $row = Items::where('id','=',$v)->first();
			  $item[] = $row['name'];
			  $description[] = $row['description'];
			  $price .= $row['price'].",";
			  $incrItem .= $incr[$k].",";
			  $k++;
			}
			
			$input['store'] = $request->session()->get('company_id');
			$input['payment'] = $request->post('payment');
			$input['price'] = substr($price, 0, -1);
			$input['item'] = json_encode($item);
			$input['description'] = json_encode($description);
			$input['incr'] = substr($incrItem, 0, -1);
			$input['id_user'] = $id_user;
			
			//dd($input);
			$get = transaction::create($input);
			//$save = Transaction::create(array('buyer'=>$request->input('buyer'),'item_id'=>$input_id,'tot'=>$request->input('tot')));
			//$id_trans = Product::create($request->all());
			//return response()->json(Item::where('item_code','=',$code)->first());
			$generator = new \Picqer\Barcode\BarcodeGeneratorJPG();
			//return response()->json('<img src="data:image/png;base64,' . base64_encode($generator->getBarcode("tr-".$get->id."-".date('Ymd'), $generator::TYPE_CODE_128)).'">');
			
		
			// Output the image
			//imagejpeg('<img src="data:image/png;base64,' . base64_encode($generator->getBarcode("tr-".$get->id."-".date('Ymd'), $generator::TYPE_CODE_128)).'">'); 
			 $source = "T".$get->id;
			 
			 $generator = new \Picqer\Barcode\BarcodeGeneratorJPG();
			 
			 $content = file_get_contents('data:image/jpg;base64,' . base64_encode($generator->getBarcode($source, $generator::TYPE_CODE_128)));
			 //$img = imagecreatefrompng('data:image/png;base64,' . base64_encode($generator->getBarcode("tr-".$get->id."-".date('YmdHis'), $generator::TYPE_CODE_128)));
			 
			 file_put_contents('gbr/'.$source.".jpg",$content);
			 
			 $img = Image::make(public_path('img/white.jpg'));  

			 $img->text("T".$get->id, 420, 100);  

			 $img->save(public_path('gbr/txt'.$source.".jpg"));  
			 
			 $img = Image::make(public_path('gbr/txt'.$source.".jpg"));

			 $img->insert(public_path('gbr/'.$source.".jpg"), 'top-center', -90, 60);

			 $img->save(public_path('gbr/'.$source.".jpg")); 
			 
			 unlink('gbr/txt'.$source.".jpg");
			
			 return response()->json($source);
		
	}
	
	function exit_trans(Request $request,$id='')
	{
		Auth::logout();
		Session::flush();
		//session_unset();
		//$data = $this->set_session($request);
		return view('payment_success')->with('data',$id);
	}
	
	function show_trans($id)
	{
		//$data = Transaction::where('id_user','=',$id)->get();
		$data = (Transaction::select(['*.transactions','companies.name as store_name'])->join('companies', 'companies.id', '=', 'transactions.store')->where("id",$id)->get())[0];
		//dd($data);
		return view('show_trans')->with('data', $data);
	}
	
	function remove(Request $request,$id="")
    {
	   $data = $request->session()->get('chart');	
	   $incr = $request->session()->get('incr');	
	   unset($data[$id]);  
	   unset($incr[$id]);  
	   $request->session()->put('chart', array_values($data));
	   $request->session()->put('incr', array_values($incr));
	   return redirect('item/shopping_chart/');

    }
	
	public function exportTransPDF($id='')
    {
        $data = (Transaction::where("id",$id)->get())[0];
        $pdf = PDF::loadView('show_trans', compact('data'));
//		$pdf->save(storage_path().'laporan_trans_'.date('Y-m-d_H-i-s').'.pdf');
        return $pdf->download('laporan_trans_'.date('Y-m-d_H-i-s').'.pdf');
    }
	
	function scan_trans(Request $request){
		
		$data = $this->set_session($request);
		return view('scan_trans')->with('data', $data);
	}
	
}

<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Excel;
use App\items;
use App\Imports\ItemsImport;
use Illuminate\Http\Request;

class itemBatchUploadController extends Controller
{
    public function importData(Request $request)
    {
         Excel::import(new ItemsImport,request()->file('file'));
		 return back();
	}	
	
	public function index(){
		return view('item/index_show_batch');
		
	}
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\UserBusiness;

use App\items;

use App\Company;

class UserBusinessController extends Controller
{
    function login()
	{
		return view('business.businessLogin');
		
	}
	
	function home(Request $request)
	{
		if(!$request->session()->get('company_id')){
			return  redirect('businessLogin');
		}
		
		$data = items::all()->where('company_id',$request->session()->get('company_id'));
		$company = Company::all()->where('id',$request->session()->get('company_id'))->first();
		return view('business.businessHome',compact('data', 'company'));
		
	}
	
	public function logintobusiness(Request $request)
	{
		$user = new UserBusiness();
		$get_data = $user->logintobusiness($request->post());
		//dd($get_data->dir_url);
	
		if($get_data)
		{
		//	dd($get_data->dir_url);
			if($get_data->company_id)
			{	
		        $request->session()->put('company_id', $get_data->company_id);
				$request->session()->put('user', $request->post('user'));
				//dd($get_data->dir_url);
				return redirect('businesshome');
			}
			else
			{
			    return  redirect('businessLogin');
			}
		}
		else
		{
			return  redirect('businessLogin');
		}
	
	}
	
	public function businesslogout(Request $request)
	{
		
		$request->session()->flush();
	
		return  redirect('businessLogin');
	}
}

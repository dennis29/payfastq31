<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use DB;

class theadmin extends Model
{
    protected $table = 'theadmins';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['email', 'password', 'role_id'];
	
	public function logintomenu($array){
		
	 //DB::enableQueryLog();	
	 return  DB::table($this->table)
		->select('*')
		->where('email',$array['email'])->where('password', $array['password'])
		->first();
	//dd($data);
	}
}

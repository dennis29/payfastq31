<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use DB;

class UserBusiness extends Model
{
    protected $fillable = ['username', 'password' ,'company_id'];
	
	public function logintobusiness($array){
		
	//DB::enableQueryLog();	
	 return DB::table('user_businesses')
		->select('*')
		->where('user',$array['user'])->where('password', $array['password'])
		->first();
		//dd(DB::getQueryLog());
	}
}

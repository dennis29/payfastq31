INSERT INTO activity_log VALUES (1, 'default', 'App\Company model has been created', 1, 'App\Company', 1, 'App\User', '[]', '2019-11-18 16:15:16', '2019-11-18 16:15:16');
INSERT INTO activity_log VALUES (2, 'default', 'App\Company model has been created', 2, 'App\Company', 1, 'App\User', '[]', '2019-11-18 16:15:46', '2019-11-18 16:15:46');
INSERT INTO activity_log VALUES (3, 'default', 'App\Company model has been created', 3, 'App\Company', 1, 'App\User', '[]', '2019-11-18 16:18:44', '2019-11-18 16:18:44');


--
-- Data for Name: companies; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO companies VALUES (1, '2019-11-18 16:15:16', '2019-11-18 16:15:16', NULL, 'UNIQLO', 'https://i.postimg.cc/06QxZTZs/uniqlo.png');
INSERT INTO companies VALUES (2, '2019-11-18 16:15:46', '2019-11-18 16:15:46', NULL, 'H&M', 'https://i.postimg.cc/21TrQkQG/handm.png');
INSERT INTO companies VALUES (3, '2019-11-18 16:18:44', '2019-11-18 16:18:44', NULL, 'ZARA', 'https://i.postimg.cc/VSVwhKHb/zara.png');


--
-- Data for Name: customers; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Data for Name: employees; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Data for Name: items; Type: TABLE DATA; Schema: public; Owner: postgres
--
/*
INSERT INTO items VALUES (1, 'AB233', 'A0001', 'White Shirt', 'White as paper', 5, 'https://i.postimg.cc/SsJ0YWK2/whiteshirt.jpg', '1', NULL, NULL, 1);
INSERT INTO items VALUES (2, 'AB235', 'A0002', 'Jeans', 'Jeans as light as feather', 3, 'https://i.postimg.cc/DzCkzC73/jeans.jpg', '1', NULL, NULL, 1);

*/
INSERT INTO items VALUES (1,1,'AB233', 'A0001','White Shirt', 'White as paper', 5, 'https://i.postimg.cc/SsJ0YWK2/whiteshirt.jpg', '1', NULL, NULL);
INSERT INTO items VALUES (2,1,'AB235','A0002','Jeans', 'Jeans as light as feather', 3, 'https://i.postimg.cc/DzCkzC73/jeans.jpg', '1', NULL, NULL);

--
-- Data for Name: migrations; Type: TABLE DATA; Schema: public; Owner: postgres
--

--
-- Data for Name: pages; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO pages VALUES (1, NULL, NULL, NULL, 'users', NULL);


--
-- Data for Name: password_resets; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Data for Name: payments; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Data for Name: permission_role; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO permission_role VALUES (1, 2);
INSERT INTO permission_role VALUES (1, 1);
INSERT INTO permission_role VALUES (3, 1);
INSERT INTO permission_role VALUES (2, 1);
INSERT INTO permission_role VALUES (4, 1);
INSERT INTO permission_role VALUES (5, 1);
INSERT INTO permission_role VALUES (6, 1);
INSERT INTO permission_role VALUES (7, 1);
INSERT INTO permission_role VALUES (8, 1);
INSERT INTO permission_role VALUES (9, 1);
INSERT INTO permission_role VALUES (10, 1);
INSERT INTO permission_role VALUES (11, 1);
INSERT INTO permission_role VALUES (12, 1);


--
-- Data for Name: permissions; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO permissions VALUES (1, 'Permissions', 'Permissions', NULL, NULL);
INSERT INTO permissions VALUES (3, 'Users', 'Users', NULL, NULL);
INSERT INTO permissions VALUES (2, 'Roles', 'Roles', NULL, NULL);
INSERT INTO permissions VALUES (4, 'Pages', 'Pages', NULL, NULL);
INSERT INTO permissions VALUES (5, 'Activity Logs', 'Activity Logs', NULL, NULL);
INSERT INTO permissions VALUES (6, 'Settings', 'Settings', NULL, NULL);
INSERT INTO permissions VALUES (7, 'Employee', 'Employee', NULL, NULL);
INSERT INTO permissions VALUES (8, 'product', 'product', NULL, NULL);
INSERT INTO permissions VALUES (9, 'generator', 'generator', NULL, NULL);
INSERT INTO permissions VALUES (10, 'Item Batch', 'Item Batch', NULL, NULL);
INSERT INTO permissions VALUES (11, 'Generator', 'Generator', NULL, NULL);
INSERT INTO permissions VALUES (12, 'Company', 'Company', NULL, NULL);


--
-- Data for Name: products; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Data for Name: role_user; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO role_user VALUES (1, 1);


--
-- Data for Name: roles; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO roles VALUES (1, 'Super Admin', 'Super Admin', NULL, NULL);


--
-- Data for Name: sessions; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Data for Name: settings; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Data for Name: stores; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Data for Name: transactions; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Data for Name: user_role; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO user_role VALUES (1, 1, NULL, NULL);


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO users VALUES (1, 'manullang_d@yahoo.com', '$2y$10$wblQE3PVdhoWzOtIDEeJfeb53nkhCdl7Lgpn08IkDGQD5dvVCkM2W', 'michael dennis', NULL, NULL, NULL, NULL, '2019-11-17 06:46:11', '2019-11-17 06:46:11');


INSERT INTO theadmins VALUES (1, 'manullang_d@yahoo.com', 'dennismaindeveloper', 'dennis', 1, 1, NULL, NULL, NULL, NULL);